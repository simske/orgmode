#!/usr/bin/env python3

from orgmodepy import Note
import argparse
import datetime
import os

parser = argparse.ArgumentParser()
parser.add_argument('mode')
parser.add_argument('file')
args = parser.parse_args()

tmp_done_filename = 'tmp_done.org'

if args.mode == 'upcoming':
    tage = Note(file=args.file)

    # load days in upcoming and done lists
    upcoming = []
    done = []
    for subnote in tage.children:
        if subnote.state == 'NOTE':
            upcoming.append(subnote)
        else:
            done.append(subnote)


    # parse dates
    upcoming_dates = [datetime.date(int(t.title[:4]), int(t.title[5:7]), int(t.title[8:10])) for t in upcoming]

    # create notes for next 14 days if they don't exist
    today = datetime.date.today()
    dayofweek = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
    for i in range(28):
        day = today + datetime.timedelta(i)
        if day not in upcoming_dates:
            title = day.isoformat() + " " + dayofweek[day.weekday()]
            upcoming.append(Note(title))

    # save to file
    upcoming.sort(key=lambda x: x.title)
    tage.children = upcoming
    tage.save()

    done_file = Note('Done')
    done_file.children = done
    done_file.save(tmp_done_filename)

elif args.mode == 'done':
    done = Note(file=args.file)
    done_new = Note(file=tmp_done_filename)
    done.children = done_new.children + done.children
    done.children.sort(key=lambda x: x.title, reverse=True)
    done.save()
    os.remove(tmp_done_filename)

else:
    raise RuntimeError("Modes: upcoming, done")
