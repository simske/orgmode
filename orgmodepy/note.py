import re

__all__ = ['Note', 'printnote']

printnote = lambda node : print(node.str_full())

class Note:
    """
    Orgmode Note:
    represents one Orgmode node and it's subnotes. Can also represent complete file
    """
    def __init__(self, title=None, content: list=[], file=None):
        self.content = [c for c in content if c != '']
        self.children = []
        self.state = 'FILE'
        if title is not None:
            titlematch = re.match('^(TODO|DONE|NEXT) (.*)$', title)
            if titlematch is None:
                self.title = title
                self.state = 'NOTE'
            else:
                self.title = titlematch.group(2)
                self.state = titlematch.group(1)
        elif file is not None:
            self.from_file(file)
        else:
            raise("You cannot create empty note")

    def from_file(self, file: str):
        # read und clean lines
        with open(file) as f:
            lines = [line.strip() for line in f.readlines()]

        # search for note headlines (and corresponding line numbers)
        eintrag_search = re.compile('^(\*+)(.*)')
        eintrag_res = []
        eintrag_index = []
        for i, line in enumerate(lines):
            res = eintrag_search.match(line)
            if res is not None:
                eintrag_res.append(res)
                eintrag_index.append(i)

        # root note
        self.title = file

        # save last note for every level
        parents = {}
        for start, end, res in zip(eintrag_index, eintrag_index[1:] + [-1], eintrag_res):
            level = len(res.group(1))
            eintrag = Note(res.group(2).strip(), content=lines[start+1:end])
            if level > 1:
                parents[level-1].add_child(eintrag)
            else:
                self.add_child(eintrag)
            parents[level] = eintrag

    def add_child(self, new_child):
        """Add child note"""
        self.children.append(new_child)

    def __str__(self):
        if self.state == "NOTE":
             return f"{self.title}"
        else:
            return f"{self.state} {self.title}"

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, index):
        return self.children[index]

    def str_full(self, indent="  "):
        """Note with full subtree as string"""
        def str_action(node, level, kwargs):
            kwargs['out'] += indent * level + str(node) + "\n"
        kwargs = {'out': ''}
        self._dfs(str_action, self, kwargs)
        return kwargs['out']

    def __lt__(self, other):
        return self.title < other.title


    def _dfs(self, action, node, action_args={}, level=0):
        """
        Method to traverse full subtree depth-first and running `action`
        on node.
        dict `action_args` can be used to carry objects while traversing
        """
        action(node, level, action_args)
        for child in node.children:
            self._dfs(action, child, action_args, level+1)

    def save(self, file=None):
        """Save into org-file. If filename is not given, title of root node is used"""
        if file is None:
            file = self.title

        def save_action(node, level, kwargs):
            f = kwargs['f']
            if level != 0:
                if level == 1 and f.tell() != 0:
                    f.write('\n')
                f.write('*'*level +
                        f" {node.state+' ' if node.state != 'NOTE' else ''}{node.title}\n")
            for line in node.content:
                f.write(line + "\n")
            if node.content:
                f.write("\n")

        with open(file, 'w') as f:
            self._dfs(save_action, self, {'f':f})

    def todo(self):
        self.state = 'TODO'
    def next(self):
        self.state = 'NEXT'
    def done(self):
        self.state = 'DONE'
    def note(self):
        self.state = 'NOTE'
