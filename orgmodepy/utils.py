import argparse

from .note import Note

__all__ = ['sort_done', 'sort_done_script']

def sort_done(note: Note, subsort: bool=False):
    def action(note: Note, *_, **__):
        todo, done = [], []
        for subnote in note.children:
            if subnote.state == "DONE":
                done.append(subnote)
            else:
                todo.append(subnote)
        note.children = todo + done

    if subsort:
        note._dfs(action, note)
    else:
        action(note)

def sort_done_script():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('-s', '--subsort', action='store_true')
    args = parser.parse_args()

    orgfile = Note(file=args.file)
    sort_done(orgfile, args.subsort)
    orgfile.save()
